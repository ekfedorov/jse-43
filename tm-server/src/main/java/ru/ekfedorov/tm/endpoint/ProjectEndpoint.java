package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.IProjectEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;
import ru.ekfedorov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public ProjectDTO addProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectDTOService().add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearBySessionProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException, NullSessionException{
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        serviceLocator.getProjectDTOService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAllWithComparator(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectDTOService().findAll(session.getUserId(), sort);
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectDTOService().findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectDTOService().findOneById(session.getUserId(), id).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectDTOService().findOneByIndex(session.getUserId(), index).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectDTOService().findOneByName(session.getUserId(), name).orElse(null);
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "project", partName = "project") @NotNull ProjectDTO project
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().remove(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectByIdWithTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectTaskDTOService().removeProjectById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectDTOService().updateByIndex(session.getUserId(), index, name, description);
    }

}
