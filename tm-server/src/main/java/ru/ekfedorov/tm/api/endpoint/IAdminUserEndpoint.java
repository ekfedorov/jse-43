package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.dto.UserDTO;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void clearUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException;

    @WebMethod
    void createUserWithRole(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<UserDTO> findAllUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    UserDTO findUserOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException;

    @WebMethod
    void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AccessDeniedException, UserIsLockedException;

    @WebMethod
    void removeByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AccessDeniedException;

    @WebMethod
    void removeUserOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException;

    @WebMethod
    void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AccessDeniedException;

}
