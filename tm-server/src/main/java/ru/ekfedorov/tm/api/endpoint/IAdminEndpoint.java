package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminEndpoint {

    @WebMethod
    void clearProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException;

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException;

    @WebMethod
    List<SessionDTO> listSession(
            @WebParam(name = "session", partName = "session"
            ) @Nullable final SessionDTO session
    ) throws AccessDeniedException;

}
