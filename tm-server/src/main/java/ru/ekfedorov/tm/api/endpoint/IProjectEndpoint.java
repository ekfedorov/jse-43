package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.ProjectDTO;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    ProjectDTO addProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void clearBySessionProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<ProjectDTO> findProjectAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @NotNull String sort
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    ProjectDTO findProjectOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    ProjectDTO findProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    ProjectDTO findProjectOneByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void finishProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void finishProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "project", partName = "project") @NotNull ProjectDTO project
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeProjectByIdWithTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeProjectOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeProjectOneByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void startProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void startProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

}
