package ru.ekfedorov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IRepositoryDTO;
import ru.ekfedorov.tm.dto.UserDTO;

import java.util.List;
import java.util.Optional;

public interface IUserDTORepository extends IRepositoryDTO<UserDTO> {

    void clear();

    @NotNull
    List<UserDTO> findAll();

    @NotNull
    Optional<UserDTO> findByLogin(@NotNull String login);

    @NotNull
    Optional<UserDTO> findOneById(@Nullable String id);

    void removeByLogin(@NotNull String login);

    void removeOneById(@Nullable String id);

}
