package ru.ekfedorov.tm.api.entity;

public interface IWBS extends IHasName, IHasCreated, IHasDateStart, IHasDateFinish, IHasStatus {
}
