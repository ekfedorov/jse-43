package ru.ekfedorov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Column
    @Nullable
    private String signature;

    @Column
    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
