package ru.ekfedorov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IWBS;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_task")
public class TaskDTO extends AbstractBusinessEntityDTO implements IWBS {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
