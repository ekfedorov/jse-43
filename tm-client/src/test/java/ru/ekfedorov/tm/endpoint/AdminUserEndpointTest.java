package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class AdminUserEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private SessionDTO sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithRole() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        adminUserEndpoint.createUserWithRole(sessionAdmin, "testCreateWR", "test2", Role.ADMIN);
        final UserDTO user = endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "testCreateWR");
        Assert.assertEquals(Role.ADMIN, user.getRole());
        adminUserEndpoint.removeByLogin(sessionAdmin, "testCreateWR");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllUser() {
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint().findAllUser(sessionAdmin).isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserOneById() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        Assert.assertNotNull(adminUserEndpoint.findUserOneById(sessionAdmin, sessionAdmin.getUserId()));
    }

    @Test(expected = UserIsLockedException_Exception.class)
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByLogin() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        adminUserEndpoint.lockUserByLogin(sessionAdmin, "test");
        endpointLocator.getSessionEndpoint().openSession("test", "test");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeByLogin() {
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "testRemoveByLogin");
        endpointLocator.getUserEndpoint().createUser("testRemoveByLogin", "test", "test@twst.ru");
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "testRemoveByLogin");
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "testRemoveByLogin"));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserOneById() {
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "testRemoveById");
        endpointLocator.getUserEndpoint().createUser("testRemoveById", "test", "test@twst.ru");
        final UserDTO user = endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "testRemoveById");
        endpointLocator.getAdminUserEndpoint().removeUserOneById(sessionAdmin, user.getId());
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "testRemoveById"));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLogin() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        adminUserEndpoint.lockUserByLogin(sessionAdmin, "test");
        adminUserEndpoint.unlockUserByLogin(sessionAdmin, "test");
        Assert.assertFalse(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test").isLocked());
    }

}
