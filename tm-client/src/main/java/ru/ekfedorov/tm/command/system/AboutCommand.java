package ru.ekfedorov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display developer info.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "about";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}
